import axios, {AxiosPromise} from "axios";

const getFilmsByCategory = (category:string = "girls"):Promise<AxiosPromise> => {
    return axios.get("https://api.tvmaze.com/search/shows?q="+ category);
}

export default getFilmsByCategory;