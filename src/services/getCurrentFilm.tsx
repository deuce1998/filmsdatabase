import axios, {AxiosPromise} from "axios";

const getCurrentFilm = (id:string | null = null):Promise<AxiosPromise> => {
    return axios.get("https://api.tvmaze.com/shows/"+ id);
}

export default getCurrentFilm;