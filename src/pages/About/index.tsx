import aboutImg from '../../images/about.png';
import './style.scss'

const About = () => {
    return (
        <section className="about">
            <div className="container">
                <div className="main-content main-content about-content">
                    <img src={aboutImg} alt="" className="about-content__img" />
                    <div className="about-content__inner">
                        <h2 className="about-content__header">MOVIESinfo</h2>
                        <p className="about-content__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. </p>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default About;