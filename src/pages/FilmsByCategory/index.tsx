import {useState,useEffect} from "react";
import { useDispatch, useSelector } from 'react-redux';
import { loadFilms } from '../../store/films/actions';
import { filmsSelector } from '../../store/films/selectors';
import { TypedDispatch } from '../../store';
import FilmsCardList from "../../components/FilmsCardList";

import  './style.scss'

const FilmsByCategory = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const filmsByCategory = useSelector(filmsSelector);
    const [category, setCategory] = useState('Family');
    
    useEffect(() => {
       {category ?
            dispatch(loadFilms(category)):
            dispatch(loadFilms()) } ;
    }, [dispatch, category]);

    return (
        <section className="films-by-category">
            <div className="container">
                <div className="main-content card-list-wrapper">
                    <p className="selected-category">Выбранная категория: 
                        <select name="select-category" id="select-category" defaultValue="Family" onChange={(event) => setCategory(event.target.value)}>
                            <option value="Family">Family</option>
                            <option value="Comedy">Comedy</option>
                            <option value="Legal">Legal</option>
                            <option value="Drama">Drama</option>
                            <option value="Science">Science</option>
                            <option value="Children">Children</option>
                            <option value="History">History</option>
                            <option value="War">War</option>
                            <option value="Crime">Crime</option>
                            <option value="Anime">Anime</option>
                        </select> 
                    </p>
                    {filmsByCategory && <FilmsCardList films={[...filmsByCategory].slice(0,12)} />}
                </div>                
            </div>
        </section>

    );
}
export default FilmsByCategory;