import {useEffect} from "react";
import { useDispatch, useSelector } from 'react-redux';
import {useParams } from "react-router-dom";
import { loadCurrentFilm } from '../../store/films/actions';
import { oneFilmSelector } from '../../store/films/selectors';
import { TypedDispatch } from '../../store';

import preloader from '../../images/loading62.gif';
import  './style.scss'


const FilmDetail = () => {
    const {filmId} = useParams<string>();
    const dispatch = useDispatch<TypedDispatch>();
    const filmDetail = useSelector(oneFilmSelector);

    useEffect(()=>{
        dispatch(loadCurrentFilm(filmId));
    },[filmId])

    return  (
        <section className="section-films-detail">
            <div className="container">
                {filmDetail ? (    
                <div className="main-content film-detail">
                    <div className="film-detail__img">
                        <img src={filmDetail.image && filmDetail.image.medium} alt={filmDetail.name}/>
                    </div>
                    <div className="film-detail__inner">
                        <div className="film-detail__title">
                            <span className="film-detail__name">{filmDetail.name && filmDetail.name}</span>
                            <span className="film-detail__score">{(filmDetail.rating && filmDetail.rating.average) ? filmDetail.rating.average : 0.0 }</span>
                        </div>
                        <div className="film-detail__content">
                            <div className="film-detail__release-date">
                                <div className="film-detail__content-row">
                                    <b>Год выхода:</b>
                                    <p className="film-detail__content-ordinary">{filmDetail.premiered && filmDetail.premiered.slice(0,4)}</p>
                                </div>
                            </div>
                            <div className="film-detail__country">
                                <div className="film-detail__content-row">
                                    <b>Страна:</b>
                                    <p className="film-detail__content-ordinary">{filmDetail.network && filmDetail.network.country.name}</p>
                                </div>
                            </div>
                            <div className="film-detail__genre">
                                <div className="film-detail__content-row">
                                    <b>Жанр:</b>
                                    <p className="film-detail__content-ordinary">{filmDetail.genres && filmDetail.genres.join(", ")}</p>
                                </div>
                            </div>
                            <div className="film-detail__description">
                                <div className="film-detail__content-row">
                                    <b>Описание:</b>
                                    <div className="film-detail__content-ordinary">{filmDetail.summary && <div dangerouslySetInnerHTML={{ __html: filmDetail.summary }} />}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                </div>
                ): (
                    <div className="main-content main-content_black">
                        <img src={preloader} alt="" />
                    </div> 
                )}
            </div>
        </section>
    )
}

export default  FilmDetail;