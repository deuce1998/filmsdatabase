import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadFilms } from '../../store/films/actions';
import { filmsSelector } from '../../store/films/selectors';
import { TypedDispatch } from '../../store';

import FilmsCardList from "../../components/FilmsCardList";


import './style.scss'

const Home = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const filmsList = useSelector(filmsSelector);

    useEffect(() => {
        dispatch(loadFilms());
    }, [dispatch]);

    return (
        <section className='main-films'>
            <div className="container">
                <div className='home-header'>
                    <h1 className='home-header__title'>MOVIESinfo</h1>
                    <p className='home-header__text'>Самый популярный портал о фильмах </p>                
                </div>                
                <div className="main-content">
                    <div className="card-list-wrapper">
                        {filmsList && <FilmsCardList films={[...filmsList].slice(0,8)} />}
                    </div>                    
                </div>           
            </div>
        </section>

    )
}
export default Home;