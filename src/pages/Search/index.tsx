import {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadFilms} from '../../store/films/actions';
import { TypedDispatch } from '../../store';
import { filmsSelector } from '../../store/films/selectors';
import FilmsCardList from '../../components/FilmsCardList';

import './style.scss';

const Search = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const filmsByCategory = useSelector(filmsSelector);
    const [genre, setGanre] = useState<string>("");
    function handleChange(value:string) {
        setGanre(value);
    }
    function handleClick() {
        {genre && dispatch(loadFilms(genre)) }
    }

    return (
        <section className='search-section'> 
            <div className="container">
                <div className="main-content search-result-wrapper">
                    <h1>Поиск по категории</h1>
                    <div className="search">
                        <button className="search__button" onClick={handleClick}></button>
                        <input className="search__input" type="text" value={genre}  
                        onChange={event => handleChange(event.target.value)}/>
                    </div>
                    <div className="search-result">
                        <h2 className="search-result__header">Результаты поиска:</h2>
                        {filmsByCategory && filmsByCategory.length > 0 ? 
                            <FilmsCardList films={[...filmsByCategory].slice(0,12)}/>:
                            <p className="text-notification search-result__notification">Введите поисковой запрос <br/>
                            для отображения результатов</p>
                        }
                    </div>                    
                </div>

            </div>
        </section>
        
    );
}

export default Search;