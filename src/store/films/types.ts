import { IFilmsItem } from "../../interface/IFilmsItem";
import {IFilmDetail} from "../../interface/IFilmDetail"

export interface IStore{
    films: IFilmsItem[],
    currentFilm:IFilmDetail | null,
}