import {IStore} from './types'

export const filmsSelector = (state: {filmsReducer: IStore}) => state.filmsReducer.films;

export const oneFilmSelector = (state: {filmsReducer: IStore}) => state.filmsReducer.currentFilm;