import { Dispatch } from "redux";
import getFilmsByCategory from "../../services/getFilmsByCategory";
import getCurrentFilm from "../../services/getCurrentFilm"
import { AnyAction } from "redux";
import { IStore } from "./types";

export const setFilmsAction = (payload:IStore['films']):AnyAction =>{
    return {
        type: 'films/setFilms',
        payload
    }
}

export const setCurrentFilmAction = (payload:IStore['currentFilm']):AnyAction =>{
    return {
        type: 'films/currentFilm',
        payload
    }
}

export const loadFilms = (category:string = "family") => async (dispatch: Dispatch) => {
    try {
        const response = await getFilmsByCategory(category);
        dispatch(setFilmsAction(response.data));
    }
    catch (e){
        console.log(e, "Ошибка в loadFilms Actions")
    }
}



export const loadCurrentFilm = (id:string | undefined) => async (dispatch:Dispatch) => {
    try{
        const response = await getCurrentFilm(id);
        dispatch(setCurrentFilmAction(response.data))
    }
    catch(e){
        console.log(e, "Ошибка в loadCurrentFilm Actions")
    }
    

}