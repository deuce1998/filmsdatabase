import { AnyAction } from "redux";
import { IStore } from "./types";


const initialState = {
    films: [],
    currentFilm: null,
}

const filmsReducer = (state: IStore = initialState, action: AnyAction) => {
    switch(action.type){
        case 'films/setFilms': 
            return {...state, films: [...action.payload]};
        case 'films/currentFilm': 
            return {...state, currentFilm: action.payload};
        default:
            return state;
    }
}

export default filmsReducer;