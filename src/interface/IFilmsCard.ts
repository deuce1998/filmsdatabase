export interface IFilmsCard {
    id: number,
    name: string,
    imageOriginal: string,
    genres: string[],
    country: string | null,
}
