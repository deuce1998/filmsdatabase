import {Routes, Route} from "react-router-dom";
import Home from '../../pages/Home';
import About from '../../pages/About';
import FilmsByCategory from '../../pages/FilmsByCategory';
import Header from '../Header';
import Footer from "../Footer";
import Search from "../../pages/Search";
import FilmDetail from "../../pages/FilmDetail";



import './reset.scss';
import './style.scss';

const App = () => {
  return (
    <div className="App">
      <Header />
      <main>
        <Routes>
          <Route path="/Search" element={<Search />}/>
          <Route path="/About" element={<About />}/>  
          <Route path="/FilmsByCategory" element={<FilmsByCategory />}/>
          <Route path="/FilmDetail/:filmId" element={<FilmDetail />}/>  
          <Route path="*" element={<Home />} />      
        </Routes>        
      </main>
      <Footer />
    </div>
  );
}

export default App;
