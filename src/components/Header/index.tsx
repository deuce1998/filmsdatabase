import {useState} from 'react';
import { NavLink } from "react-router-dom";


import './style.scss';
import './hamburger.css'

const Header = () => {
    const [toggler,setToggler] = useState<boolean>(false);
    const enableToggle = () =>{
        setToggler(!toggler);
    }

    return (
        <header>
            <div className="container">
                <div className='head'>
                    <div className="logo">
                        
                    </div>
                    <div className="menu-wrap">
                        <input type="checkbox" className="toggler" checked={toggler} onChange={enableToggle}/>
                        <div className="hamburger"><div></div></div>
                        <nav className='menu'>
                            <NavLink to="/" className={({ isActive }) => isActive ? 
                            'menu__item menu__item_active' : 'menu__item'} onClick={enableToggle}>Главная</NavLink>
                            <NavLink to="filmsByCategory" className={({ isActive }) => isActive ? 
                            'menu__item menu__item_active' : 'menu__item'} onClick={enableToggle}>Фильмы по категории</NavLink>
                            <NavLink to="about" className={({ isActive }) => isActive ? 
                            'menu__item menu__item_active' : 'menu__item'} onClick={enableToggle}>О нас</NavLink>
                            <NavLink to="search" className={({ isActive }) => isActive ? 
                            'menu__item menu__item_active' : 'menu__item'} onClick={enableToggle}>Поиск</NavLink>
                        </nav>
                    </div>                       
                </div>
            </div>
        </header>
    );
}

export default Header;
