import React from "react";
import FilmsCardItem from "./FilmsCardItem";
import { IFilmsItem } from "../../interface/IFilmsItem";

import './style.scss'

interface IFilmsList{
    films:IFilmsItem[]
}


const FilmsCardList: React.FC<IFilmsList> = ({films}:IFilmsList) => {

    return (
        <div className="cards-list">
            {films && [...films].map(film => {
               return <FilmsCardItem key={film.show.id}
                filmsCard={film}/>
            })}
           
        </div>
    );
}

export default FilmsCardList;