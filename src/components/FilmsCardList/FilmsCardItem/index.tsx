import React, {useEffect} from "react";
import { IFilmsItem } from "../../../interface/IFilmsItem";
import {Link} from "react-router-dom";

import './style.scss'

interface IFilmsList {
    filmsCard:IFilmsItem
}

const FilmsCardItem: React.FC<IFilmsList> = ({filmsCard}:IFilmsList) => {
    useEffect(() => {
        
    })

    return (
        <Link className="card-wrapper" to={"/FilmDetail/" + filmsCard.show.id}>
            <div key={filmsCard.show.id} className = "card">
                <div className="card__inner">
                    <div className="card__img-wrapper">
                    <img className="card__img" src={filmsCard.show.image && filmsCard.show.image.medium} alt={filmsCard.show.name} /> 
                    </div>
                    <div className="card__content">
                        <p className="card__title">{filmsCard.show.name && filmsCard.show.name}</p>
                        <p className="card__text card__text-country">{filmsCard.show.network && filmsCard.show.network.country.name}</p>
                        <p className="card__text">{filmsCard.show.genres && filmsCard.show.genres.join(', ')}</p>
                    </div>                
                </div>
            </div>        
        </Link>

    );
}

export default FilmsCardItem;