import logo from "../../images/logo.png"
import './style.scss';

const Footer = () => {
    return (
        <footer id='footer' className='footer'>
            <div className="container">
                <div className="footer-inner">
                    <div className="logo">
                        <img src={logo} alt="Логотип" />
                    </div>
                    <div className="footer-theme-wrapper">
                        <div className="footer-theme">
                            <p>Дипломный проект</p>    
                        </div>
                        <div className="made">
                            <p>Made by</p>
                            <p>Матюшкин Алексей</p>
                        </div>                           
                    </div>
                 
                </div>
            </div>
        </footer>
    );
}
export default Footer;